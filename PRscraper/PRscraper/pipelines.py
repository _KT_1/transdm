import mysql.connector as connection

class DatabasePipeline:
       def __init__(self):
           self.db='PRdata'
           self.user='root'
           self.passwd='groot@123'
           self.host='localhost'
       
       #@classmethod
       #def from_crawler(self,crawler):
       #       db = self.db
       #       user = self.user
       #       passwd = self.passwd
       #       host = self.host
       #       return cls(db, user, passwd, host)
       
       def open_spider(self, spider):
              self.conn = connection.connect(db=self.db,
                            user=self.user, passwd=self.passwd,
                            host=self.host,
                            charset='utf8', use_unicode=True)
              self.cursor = self.conn.cursor()
              # Insert data records into the database (one item at a time)
       
       def process_item(self, item, spider):
              sql = "INSERT INTO mit_rates( title, cost, fee, cap) VALUES (%s, %s, %s,%s)"
              for i in range(len(item.get("title"))):
                  self.cursor.execute(sql,
                                   (
                                   item.get("title")[i],
                                   item.get("cost")[i],
                                   item.get("fee")[i],
                                   item.get("cap")[i]
                                   )
                                   )
              self.conn.commit()
              return item
    # When all done close the database connection
       
       def close_spider(self, spider):
              self.conn.close()

