import scrapy
from scrapy.loader import ItemLoader
from PRscraper.items import PrscraperItem 
class MitSpider(scrapy.Spider):
    name = 'mit'
    allowed_domains = ['mit.com']
    start_urls = ['http://web.mit.edu/facilities/transportation/parking/rates.html']
    custom_settings = {"FEEDS":{"results.csv":{"format":"csv"}}}
    
    def parse(self, response):
        item_loader = ItemLoader(item=PrscraperItem(), response=response)
        for i in range(1,12):	
        	item_loader.add_xpath('title', '//*[@id="maincontent"]/table/tr[{i}]/td[1]/strong/text()'.format(i=i))
        	item_loader.add_xpath('cost', '//*[@id="maincontent"]/table/tr[{i}]/td[2]/text()'.format(i=i))
        	item_loader.add_xpath('fee', '//*[@id="maincontent"]/table/tr[{i}]/td[3]/text()'.format(i=i))
        	item_loader.add_xpath('cap', '//*[@id="maincontent"]/table/tr[{i}]/td[4]/text()'.format(i=i))
        	yield item_loader.load_item()
