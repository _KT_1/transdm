import requests
import pandas

file_url = '/Users/kt/Documents/Transportation/MBTAwebsite/transdm/smartystreets/'

credentials = {'auth-id':'7484fd9a-676f-2c50-f1a2-273e701140b8',
		    'auth-token':'2NJlN3C1QQ41oLcE9fID'}

dan_creds = {'auth-id':'6266d21d-dfa0-e6cb-a9a9-ec98b23f1da1',
                'auth-token':'X9Pt1YRROZ3qR2lXIkmX'}

doshi_creds = {'auth-id':'d42d61c3-8268-2191-af25-a26e0d30fb4e',
                 'auth-token':'UhVuY1jQSE8kc6L38NJ7'}

kt_umb_creds = {'auth-id':'64e8de93-d99a-64a4-db69-6fb6b682edf6',
                  'auth-token':'9MtchxaP84piATXUWRWG'}
def address_api(addr):
    url = "https://us-street.api.smartystreets.com/street-address?auth-id={auth_id}&auth-token={auth_token}&license=us-core-cloud&street={address}&city={city}&state={state}&zipcode={zipcode}"
    url = url.format(auth_id=kt_umb_creds['auth-id'],
                    auth_token=kt_umb_creds['auth-token'],
                    address=addr[8],city=addr[10],state=addr[11],zipcode=addr[12])
    print('URL:',url)
    response = requests.get(url)
    output = response.json()
    if output:
        output = output[0]
        addr.append(output.get('delivery_line_1','NA'))
        addr.append(output.get('components',{}).get('city_name','NA'))
        addr.append(output.get('components',{}).get('zipcode','NA'))
        addr.append(output.get('components',{}).get('state_abbreviation','NA'))
        addr.append(output.get('analysis',{}).get('dpv_footnotes','NA'))
        print(response.json())
    else:
        print('Errrrrorrrr')
    return response.json()

def check_dpv_match_code(response):
    code = response[0]['analysis']['dpv_footnotes']
    # print("ANALYSIS:",response[0]['analysis']) #no 
    if code == 'AABB':
        return True
    return False

def verify_address(addr):
    addr_true=address_api(addr)
   # # print(addr_true)
    if addr_true:
        # print(addr)
        if check_dpv_match_code(addr_true):  #direct push
            addr.append('true')
        else:
            addr[8] = addr_true[0]['delivery_line_1'] #correct the address and push
            addr.append('true')
    else:
        addr.append('false')
    
    addr.pop(9) # pop address 2
    return addr

def write_excel(filtered_lis):
    #print(filtered_lis)
    print(filtered_lis.append(['Date','Status','MBTA_Pass_Status','First_Name','Last_Name','Pass_type','Resident_Hall','Pass_mailed','Address','City','State','Postal_Code','Career','UMSID','Email','Phone','MBTA_RATE_11','MBTA_RATE_UGRD_SUBSIDIZED','Face_value','Pass_Cost','Billing_Amount','Agree_to_Terms','Acknowledgement','Verified_Address','smarty delivery line','smarty city','smarty state','smarty zipcode']))
    output = pandas.DataFrame(filtered_lis).to_excel(file_url+'dan_bug_list.xlsx', 
                                                    header=False, 
                                                    index=False, 
                                                    sheet_name='users')
    return True

def main():
    address = []
    main_file = file_url+'Spring20220118_OnlyMail.xlsx'
    mf = pandas.read_excel(main_file,sheet_name = 0)
    i = 0
    #random = mf.head(3)
    for val in mf.values.tolist():
        print(val)
        i+=1
        if str(val[8]) == 'nan': # Address 1 does not exist
           # print("came to 1")
            val.pop(9)
            address.append(val)
        elif str(val[9]) == 'nan': # Address 2 does not exist
           # print("came to 2")
            address.append(verify_address(val))
        else: # Both exists
           # print("came to 3")
            val[8] = str(val[8]) +' '+ str(val[9])
            address.append(verify_address(val))
        print(val[8])
    write_excel(address)

main()

#pop address line 2
