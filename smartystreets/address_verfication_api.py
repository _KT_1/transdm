import requests
import pandas

file_url = '/Users/kt/Documents/Transportation/MBTAwebsite/transdm/smartystreets/'

credentials = {'auth-id':'7484fd9a-676f-2c50-f1a2-273e701140b8',
		'auth-token':'2NJlN3C1QQ41oLcE9fID'}

def read_excel():
    address = []
    main_file = file_url+'mbta-pass-form.xlsx'
    mf = pandas.read_excel(main_file,sheet_name = 0,usecols="F,G,H,I,J,K")
    random = mf.head(20)
    random = random.dropna(subset=['Address1'])
    print(random)
    print(random.values.tolist(),'\n\n\n\n')
    for val in random.values.tolist():
        if str(val[2]) == 'nan':
            address.append([val[0],val[1],val[3],val[4],val[5]])
        else:
            address.append([val[0],val[1]+' '+val[2],val[3],val[4],val[5]])
    return address

def address_api(addr):
    url = "https://us-street.api.smartystreets.com/street-address?auth-id={auth_id}&auth-token={auth_token}&license=us-core-cloud&street={address}&city={city}&state={state}"
    url = url.format(auth_id=credentials['auth-id'],
                    auth_token=credentials['auth-token'],
                    address=addr[1],city=addr[2],state=addr[3])

    response = requests.get(url)
    return response.json()

def write_excel(filtered_lis):
    output = pandas.DataFrame(filtered_lis,columns=['Address','Student Email Address','Type'])
    output.to_excel(file_url+'test.xlsx', sheet_name='Sheet_name_1')

def verify_address():
    address = read_excel()
    for addr in address:
        addr_true = address_api(addr)
        print(addr[1])
        if addr_true:
           # write to new excel
           # print(addr_true[0]['analysis']['dpv_footnotes'])
            print(addr_true)
        else:
            print('wrong address')
            break

print(verify_address())
