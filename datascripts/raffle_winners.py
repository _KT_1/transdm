#AUTHOR: krishnateja.kaspe001@umb.edu
'''
Find random users from a file
with presentable UI on terminal
'''
import pandas as pd
import time

'''
Definition: read excel sheet
Input: None
Output: None
'''
def read_excel():
    file_url= '/Users/kt/Downloads/2021 UMB Prize Drawing Entries.xls'
    df = pd.read_excel(file_url,na_values=False,usecols='E',index_col=None)
    #random = df.dropna()
    sample_uniq = df.dropna(subset=['Q2.A3'])
    sample = sample_uniq.sample(25)
    write_excel(sample)
    return sample

'''
Definition: Print users with formatting and wait time between each user
Input: Excel winner list
Output: Print users with formatting and time sleep
'''
def find_winners(winners):
    for i,_ in enumerate(winners['Q2.A3']):
        print('\twinner {}'.format(i+1),_)
        time.sleep(1)
    return '\tThankyou'

'''
Definition: write data to an excel sheet
Input: users data 
Output: None
'''
def write_excel(data):
    filtered_lis = []
    filtered_lis = data.values.tolist()
    output = pd.DataFrame(filtered_lis,columns=['winners'])
    output.to_excel('/Users/kt/Documents/winners.xlsx', sheet_name='raffle_finalists')
    return

'''
Definition: maincall
Input: None
Output: call find winner function
'''
def main():
    winners = read_excel()
    print('The twentyfive lucky winners are')
    time.sleep(3)
    print('\.\.\.\.\.\.drumrolls./././././././\n\n')
    time.sleep(10)
    return find_winners(winners)

print(main())
