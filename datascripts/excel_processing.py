import requests
import pandas

file_url = '/Users/kt/Documents/Transportation/MBTAwebsite/transdm/datascripts/'

def write_excel(filtered_lis):
    #print(filtered_lis)
    print(filtered_lis.append(['Date','Status','MBTA_Pass_Status','First_Name','Last_Name','Pass_type','Resident_Hall','Pass_mailed','Address','City','State','Postal_Code','Career','UMSID','Email','Phone','MBTA_RATE_11','MBTA_RATE_UGRD_SUBSIDIZED','Face_value','Pass_Cost','Billing_Amount','Agree_to_Terms','Acknowledgement','Verified_Address']))
    output = pandas.DataFrame(filtered_lis).to_excel(file_url+'certified_mail_batch_v3.xlsx',
                                                    header=False,
                                                    index=False,
                                                    sheet_name='users')
    return True

def read_excel():
    filtered_lis = []
    main_file = file_url+'corrected_list_v3.xlsx'
    mf = pandas.read_excel(main_file,sheet_name = 0)
    #random = mf.head(3)
    for val in mf.values.tolist():
        val[12] = '0'* (5 - len(str(val[12]))) + str(val[12]) #zip
        val[14] = '0'* (8 - len(str(val[14]))) + str(val[14]) #UMBID
        val[5] = val[3] +' '+ val[4].replace("'",'') #name 
        val[9]  = val[9].replace("#",'') #address     	 
        print(val)
        filtered_lis.append(val)
    write_excel(filtered_lis)

read_excel()
