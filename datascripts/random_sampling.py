#AUTHOR: krishnateja.kaspe001@umb.edu
'''
Definition: Can give you random users from an 
            excel sheet
Input: number of users from excel
Output: writes random users output to another excel sheet
'''
import pandas 

def find_random(count):
    #find random users from  given excel
    file_name = '/Users/kt/Documents/python/20210927_DEPSurveyEmailListAll_v1.xlsx'
    df = pandas.read_excel(file_name,sheet_name = 0,usecols="D,F")
    print(df.columns)
    print(df['Student Email Address'][0])
    df["Type"] ='Student'
    sample = df.sample(count)
    return sample.to_excel('/Users/kt/Documents/surveylist3.xlsx', sheet_name='Sheet_name_1')


'''
Definition: If one columns data has to be removed from another column
            * column1 - column1  but both columns are from two different files
Input: Nones
output: Writes the difference of two files output to another excel sheet 
'''
def compare_columns():
    main_file = '/Users/kt/Documents/python/20210927_DEPSurveyEmailListAll_v1.xlsx'
    mf = pandas.read_excel(main_file,sheet_name = 1,usecols="D,A,F")
    random = mf.sample(2500)
    print(random)
    lis = []
    filtered_lis = []
    print(mf.columns)
    sample_file = '/Users/kt/Documents/python/20210928_surveylist random_sample.xlsx'
    sf = pandas.read_excel(sample_file,sheet_name = 0,usecols="A,B,C")
    for val in sf['Student Email Address']:
        lis.append(val)
    for val in random.values.tolist():
        print(val)
        if val[0] not in lis: 
            filtered_lis.append([val[0],val[1],'Employee'])

        if len(filtered_lis) == 1000:
            print('Done Thank you',len(filtered_lis))
            break

    output = pandas.DataFrame(filtered_lis,columns=['Person Name','Student Email Address','Type'])
    output.to_excel('/Users/kt/Documents/surveylist4.xlsx', sheet_name='Sheet_name_1')
    print(output)
compare_columns()
