//validation
//validation
$.metadata.setType("attr", "validate");
$(document).ready(function(){ 
    $("#freeform").validate({
        rules : { 							
                    first_name: { required : true },
                    last_name: { required : true },
                    street_1a: { required : true },
                    city: { required : true },
                    state: { required : true },
                    postal_code: { required : true },
                    student_type_grad_ug: { required : true },
                    //email: { required : true,email:true },
                    email: { isUMBEmail : true }, 
                    phone_1a: { required : true }, 
                    umsid: { required : true }, 
                    mbta_pass_type: { required : true }, 
                    short_field_001: { required : true }, 
                    short_field_002: { required : true }

                },
        messages:{
                    first_name:
                            {	required: "This field is required." },
                    last_name:
                            {	required: "This field is required." },
                    street_1a:
                            {	required: "This field is required." },
                    city:
                            {	required: "This field is required." },
                    state:
                            {	required: "This field is required." },
                    postal_code:
                            {	required: "This field is required." },
                    student_type_grad_ug:
                            {	required: "This field is required." },
                    email:
                            {	required: "Please enter a valid email address." },
                    phone_1a:
                            {	required: "This field is required." },
                    umsid:
                            {	required: "This field is required." },
                    mbta_pass_type:
                            {	required: "This field is required." },
                    short_field_001:
                            {	required: "This field is required." },
                    short_field_002:
                            {	required: "This field is required." }
                }
    }); 
});
// Validate Phone Number / Datepicker REVIEW
$(document).ready(function() {
    $.mask.definitions['~']='[+-]';
    $("#10").mask("999-999-9999");//phone_1a
});	
// UMB email
/* */
    $(document).ready(function() {
        $.validator.addMethod("isUMBEmail", function(value, element) {
            return (value.indexOf("umb.edu") > 0) ? true : false;
                }, "Please enter a UMass Boston email address.");		
    });		
/*
    show/hide
*/

$(function() {
    $("#freeform_yes_no_001_1").change(function() {
        var checked =this.checked
        if(checked){
            $("#5").removeClass("required error");//street_1a
            $("#7").removeClass("required error");//city
            $("#8").removeClass("required error");//state
            $("#9").removeClass("required error");//postal_code
            $("#10").removeClass("required error");//insurance
            $("#freeform_yes_no_002a_1").prop( "checked", false );
            $("#freeform_yes_no_002a_2").prop( "checked", false );            
            $(".resident").show();
            $(".not-resident").hide();
        }
    }).change();
    $("#freeform_yes_no_001_2").change(function() {
        var checked =this.checked
        if(checked){
            $("#5").addClass("required error");//street_1a
            $("#5").val("");//street_1a
            $("#6").val("");//street_1b
            $("#7").addClass("required error");//city
            $("#7").val("");//city
            $("#8").addClass("required error");//state
            $("#8").val("");//state
            $("#9").addClass("required error");//postal_code
            $("#9").val("");//postal_code
            $("#10").addClass("required error");//insurance
            $("#10").val("");//insurance
            $("#freeform_yes_no_002a_1").addClass("required error");
            $(".resident").hide();
            $(".not-resident").show();
            $("#freeform_yes_no_002a_1").change(function() {
                var checked =this.checked
                if(checked){
                    $(".not-mailed").hide();
                    
                }
            }).change();
            $("#freeform_yes_no_002a_1").change(function() {
                var checked =this.checked
                if(checked){
                    $(".not-mailed").hide();
                    $(".mailed").show();
                    
                }
            }).change();
            $("#freeform_yes_no_002a_2").change(function() {
                var checked =this.checked
                if(checked){
                    $(".not-mailed").show();
                    $(".mailed").hide();
                    
                }
            }).change();
        }
    }).change();
});
/*
    Insurance Yes
*/
    $("#freeform_yes_no_1").change(function() {

    $('#1015').change(function() {
        $('#919').val($(this).find('option:selected').attr('mbta_11'));//mbta_11
        var mbta_11_var = Number($("#919").val());
        $('#920').val($(this).find('option:selected').attr('mbta_50'));//mbta_50
        var mbta_50_var = Number($("#920").val());
        $('#921').val($(this).find('option:selected').attr('face_value'));//face_value
        //$('#922').val($(this).find('option:selected').attr('pass_cost'));//pass_cost
        $('#923').val($(this).find('option:selected').attr('billing_amount'));//billing_amount
        var discount_available_original = Number($("#discount_remaining").val());
        $('#924').val($(this).find('option:selected').attr('insurance'));//insurance
        var insurance_var = Number($("#924").val());
        console.log("discount_available_original: " + discount_available_original);
        console.log("mbta_11_var: " + mbta_11_var);
        console.log("mbta_50_var: " + mbta_50_var);
        console.log("new discount available: " + (discount_available_original - mbta_50_var));
        console.log("insurance_var: " + insurance_var);
        var career_var = $("#74").val();//short_field_003
        console.log("career_var: " + career_var);
        //if ((discount_available_original > mbta_50_var) && (career_var === "UGRD")) {
        if ((discount_available_original >= mbta_50_var) && (career_var == "UGRD")) {
            console.log("discount is available!**");
            $('#922').val(mbta_50_var+insurance_var);
        } else {
            console.log("discount is NOT available!");
            $('#922').val(mbta_11_var + insurance_var);
            $('#923').val(0);
        }
        
      }).change();
     }); 

    
      // Insurance No

$("#freeform_yes_no_2").change(function() {


    $('#1015').change(function() {
        $('#919').val($(this).find('option:selected').attr('mbta_11'));//mbta_11
        var mbta_11_var = Number($("#919").val());
        $('#920').val($(this).find('option:selected').attr('mbta_50'));//mbta_50
        var mbta_50_var = Number($("#920").val());
        $('#921').val($(this).find('option:selected').attr('face_value'));//face_value
        //$('#922').val($(this).find('option:selected').attr('pass_cost'));//pass_cost
        $('#923').val($(this).find('option:selected').attr('billing_amount'));//billing_amount
        var discount_available_original = Number($("#discount_remaining").val());
        console.log("discount_available_original: " + discount_available_original);
        console.log("mbta_11_var: " + mbta_11_var);
        console.log("mbta_50_var: " + mbta_50_var);
        console.log("new discount available: " + (discount_available_original - mbta_50_var));
        var career_var = $("#74").val();//short_field_003
        console.log("career_var: " + career_var);
        //if ((discount_available_original > mbta_50_var) && (career_var === "UGRD")) {
        if ((discount_available_original >= mbta_50_var) && (career_var == "UGRD")) {
            console.log("discount is available!**");
            $('#922').val(mbta_50_var);
        } else {
            console.log("discount is NOT available!");
            $('#922').val(mbta_11_var);
            $('#923').val(0);
        }
        
      }).change();
});

// Resident

$("#freeform_yes_no_001_1").change(function() {
$('#1015').change(function() {
    $('#919').val($(this).find('option:selected').attr('mbta_11'));//mbta_11
    var mbta_11_var = Number($("#919").val());
    $('#920').val($(this).find('option:selected').attr('mbta_50'));//mbta_50
    var mbta_50_var = Number($("#920").val());
    $('#921').val($(this).find('option:selected').attr('face_value'));//face_value
    //$('#922').val($(this).find('option:selected').attr('pass_cost'));//pass_cost
    $('#923').val($(this).find('option:selected').attr('billing_amount'));//billing_amount
    var discount_available_original = Number($("#discount_remaining").val());
    console.log("discount_available_original: " + discount_available_original);
    console.log("mbta_11_var: " + mbta_11_var);
    console.log("mbta_50_var: " + mbta_50_var);
    console.log("new discount available: " + (discount_available_original - mbta_50_var));
    var career_var = $("#74").val();//short_field_003
    console.log("career_var: " + career_var);
    //if ((discount_available_original > mbta_50_var) && (career_var === "UGRD")) {
    if ((discount_available_original >= mbta_50_var) && (career_var == "UGRD")) {
        console.log("discount is available!**");
        $('#922').val(mbta_50_var);
    } else {
        console.log("discount is NOT available!");
        $('#922').val(mbta_11_var);
        $('#923').val(0);
    }
    
  }).change();
});

//OfficePickup

$("#yes_no_002a").change(function() {
    $('#1015').change(function() {
        $('#919').val($(this).find('option:selected').attr('mbta_11'));//mbta_11
        var mbta_11_var = Number($("#919").val());
        $('#920').val($(this).find('option:selected').attr('mbta_50'));//mbta_50
        var mbta_50_var = Number($("#920").val());
        $('#921').val($(this).find('option:selected').attr('face_value'));//face_value
        //$('#922').val($(this).find('option:selected').attr('pass_cost'));//pass_cost
        $('#923').val($(this).find('option:selected').attr('billing_amount'));//billing_amount
        var discount_available_original = Number($("#discount_remaining").val());
        console.log("discount_available_original: " + discount_available_original);
        console.log("mbta_11_var: " + mbta_11_var);
        console.log("mbta_50_var: " + mbta_50_var);
        console.log("new discount available: " + (discount_available_original - mbta_50_var));
        var career_var = $("#74").val();//short_field_003
        console.log("career_var: " + career_var);
        //if ((discount_available_original > mbta_50_var) && (career_var === "UGRD")) {
        if ((discount_available_original >= mbta_50_var) && (career_var == "UGRD")) {
            console.log("discount is available!**");
            $('#922').val(mbta_50_var);
        } else {
            console.log("discount is NOT available!");
            $('#922').val(mbta_11_var);
            $('#923').val(0);
        }
        
      }).change();
    });

/* */
/*
919	    total_number_001        MBTA RATE 11    NOT VISIBLE (col3)
920	    total_number_002        MBTA RATE 50    Subsidized amount - NOT VISIBLE
921	    total_number_003        FACE VALUE      50% Discount (col2)
922	    total_number_004        Total Cost (col2)
923	    total_number_005        BILLING Amount
*/
